code_projects = [ "opentestfactory/agent",
                  "opentestfactory/ci-common/python-ci",
                  "opentestfactory/ci-common/java-ci",
                  "opentestfactory/java-param-library",
                  "opentestfactory/java-plugins",
                  "opentestfactory/java-toolkit",
                  "opentestfactory/jenkins-plugin",
                  "opentestfactory/orchestrator",
                  "opentestfactory/python-plugins",
                  "opentestfactory/python-toolkit",
                  "opentestfactory/qualitygate",
                  "opentestfactory/schemas",
                  "opentestfactory/tools",
                ]

doc_projects = [ "opentestfactory/site",
                 "squashtest/doc/squash-autom-devops-doc-fr",
                 "squashtest/doc/squash-autom-devops-doc-en"
               ]
