import gitlab
import sys

# -- helper functions
labels = None
def getLabelId(labelName):
    global labels
    if labels is None:
        labels = gitlabProject.labels.list(all=True)
    return list(filter(lambda l: l.name == labelName, labels))[0].id

# -- command line parsing and setup
if len(sys.argv) != 3:
    print(f'syntax: {sys.argv[0]} <token> <project ID>')
    exit(1)
token = sys.argv[1]
projectID = sys.argv[2]
try:
    gl = gitlab.Gitlab('https://gitlab.com/', private_token = token)
    gitlabProject = gl.projects.get(projectID)
except gitlab.exceptions.GitlabAuthenticationError:
    print('invalid token')
    exit(1)
except gitlab.exceptions.GitlabGetError:
    print('invalid project ID')
    exit(1)

# -- create the boards
board = gitlabProject.boards.create({'name': 'User Stories'})
board.labels = 'Type::UserStory'
board.save()
board.lists.create({'label_id': getLabelId('StatusU::New')})
board.lists.create({'label_id': getLabelId('StatusU::ImplemInProgress')})
board.lists.create({'label_id': getLabelId('StatusU::ReadyForDemo')})
board.lists.create({'label_id': getLabelId('StatusU::ImplemFinished')})
board.lists.create({'label_id': getLabelId('StatusU::ReadyForTest')})
board.lists.create({'label_id': getLabelId('StatusU::TestInProgress')})
board.lists.create({'label_id': getLabelId('StatusU::ToBeCorrected')})
board.lists.create({'label_id': getLabelId('StatusU::Validated')})
board.lists.create({'label_id': getLabelId('StatusU::Suspended')})
board.lists.create({'label_id': getLabelId('StatusU::Cancelled')})

board = gitlabProject.boards.create({'name': 'Bugs'})
board.labels = 'Type::Bug'
board.save()
board.lists.create({'label_id': getLabelId('StatusB::New')})
board.lists.create({'label_id': getLabelId('StatusB::FixInProgress')})
board.lists.create({'label_id': getLabelId('StatusB::ReadyForTest')})
board.lists.create({'label_id': getLabelId('StatusB::TestInProgress')})
board.lists.create({'label_id': getLabelId('StatusB::TestKo')})
board.lists.create({'label_id': getLabelId('StatusB::Validated')})
board.lists.create({'label_id': getLabelId('StatusB::Feedback')})
board.lists.create({'label_id': getLabelId('StatusB::Suspended')})
board.lists.create({'label_id': getLabelId('StatusB::Cancelled')})

board = gitlabProject.boards.create({'name': 'Tasks'})
board.labels = 'Type::Task'
board.save()
board.lists.create({'label_id': getLabelId('StatusT::New')})
board.lists.create({'label_id': getLabelId('StatusT::InProgress')})
board.lists.create({'label_id': getLabelId('StatusT::Done')})
board.lists.create({'label_id': getLabelId('StatusT::Suspended')})
board.lists.create({'label_id': getLabelId('StatusT::Cancelled')})

board = gitlabProject.boards.create({'name': 'Studies'})
board.labels = 'Type::Study'
board.save()
board.lists.create({'label_id': getLabelId('StatusS::New')})
board.lists.create({'label_id': getLabelId('StatusS::InProgress')})
board.lists.create({'label_id': getLabelId('StatusS::Finished')})
board.lists.create({'label_id': getLabelId('StatusS::ToRework')})
board.lists.create({'label_id': getLabelId('StatusS::Validated')})
board.lists.create({'label_id': getLabelId('StatusS::Suspended')})
board.lists.create({'label_id': getLabelId('StatusS::Cancelled')})
