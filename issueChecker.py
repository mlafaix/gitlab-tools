import argparse
import html
import os
import sys
import webbrowser

import requests

from projectLists import code_projects, doc_projects


# Command line parameters

parser = argparse.ArgumentParser()
parser.add_argument("token", help="GitLab token with API access")
parser.add_argument("-hr", "--htmlreport", help="define name of the HTML report file")
parser.add_argument(
    "-hs",
    "--htmlstring",
    help="define name of the file for recording the errors as an HTML string",
)
parser.add_argument(
    "-ts",
    "--textstring",
    help="define name of the file for recording the errors as a text string",
)
parser.add_argument(
    "-cf",
    "--culpritfile",
    help="define name of the text file for recording the culprits",
)
parser.add_argument(
    "-i",
    "--interactive",
    help="display the HTML report file (requires the -hr option)",
    action="store_true",
)
parser.add_argument(
    "-f", "--fix", help="try to fix missing labels", action="store_true"
)
parser.add_argument("-cb", "--createdby", help="a comma-separated list of usernames")
args = parser.parse_args()

CREATORS = args.createdby.split(",") if args.createdby else None
HEADERS = {"Authorization": "Bearer " + args.token}


# Helpers


def add_labels(project, issue, labels):
    response = requests.put(
        f"https://gitlab.com/api/v4/projects/{project}/issues/{issue}?add_labels={labels}",
        headers=HEADERS,
    )
    if response.status_code != 200:
        print(response.text)
        raise Exception(
            f"Failed to set label {labels} for issue {issue} in project {project}: got {response.status_code}."
        )


def run_query(query):
    request = requests.post(
        "https://gitlab.com/api/graphql", json={"query": query}, headers=HEADERS
    )
    if request.status_code == 200:
        return request.json()
    raise Exception(
        "Query failed to run by returning code of {}. {}".format(
            request.status_code, query
        )
    )


QUERY = """
{
  project(fullPath : "%s") {
    issues {
      edges {
        node {
          iid
          title
          state
          projectId
          webUrl
          mergeRequestsCount
          labels {
            edges {
              node {
                title
              }
            }
          }
          author {
            name
            username
            webUrl
          }
          createdAt
          updatedBy {
            name
            username
            webUrl
          }
          milestone {
            title
          }
          updatedAt
        }
      }
    }
  }
}
"""


# Labels helpers


def count_labels_with_scope(labels, scope):
    return sum(l.startswith(scope + "::") for l in labels)


def has_initial_code_status(labels):
    return labels.intersection(
        {"StatusU::New", "StatusB::New", "StatusT::New", "StatusS::New"}
    )


def has_canceled_code_status(labels):
    return labels.intersection(
        {
            "StatusU::Cancelled",
            "StatusB::Cancelled",
            "StatusT::Cancelled",
            "StatusS::Cancelled",
        }
    )


def has_suspended_code_status(labels):
    return labels.intersection(
        {
            "StatusU::Suspended",
            "StatusB::Suspended",
            "StatusT::Suspended",
            "StatusS::Suspended",
        }
    )


def has_finished_code_status(labels):
    return labels.intersection(
        {
            "StatusU::Validated",
            "StatusB::Validated",
            "StatusT::Done",
            "StatusS::Validated",
        }
    )


def has_final_code_status(labels):
    return (
        has_canceled_code_status(labels)
        or has_suspended_code_status(labels)
        or has_finished_code_status(labels)
    )


def has_initial_doc_status(labels):
    return "Status::New" in labels


def has_canceled_doc_status(labels):
    return "Status::Cancelled" in labels


def has_suspended_doc_status(labels):
    return "Status::Suspended" in labels


def has_final_doc_status(labels):
    return labels.intersection(
        {"Status::Done", "Status::Suspended", "Status::Cancelled"}
    )


def has_status(labels):
    return (
        count_labels_with_scope(labels, "StatusB")
        + count_labels_with_scope(labels, "StatusS")
        + count_labels_with_scope(labels, "StatusT")
        + count_labels_with_scope(labels, "StatusU")
        + count_labels_with_scope(labels, "Status")
        > 0
    )


# Checks


def check_label_unicity(issue, labels, scope, default=None):
    count = count_labels_with_scope(labels, scope)
    if count == 0:
        if args.fix and default:
            add_labels(issue["projectId"], issue["iid"], default)
            labels.add(default)
            return ["Missing " + scope + " label, " + default + " added"]
        return ["the issue has no " + scope + " label"]
    if count > 1:
        return ["the issue has several " + scope + " labels"]
    return []


def check_code_issue(issue, state, milestone, labels, createdOn, modifiedOn, mrCount):
    errors = []
    errors.extend(check_label_unicity(issue, labels, "Type"))
    errors.extend(check_label_unicity(issue, labels, "Priority", "Priority::Lowest"))
    if "Type::Bug" in labels:
        errors.extend(check_label_unicity(issue, labels, "StatusB", "StatusB::New"))
        errors.extend(check_label_unicity(issue, labels, "ProdImpact"))
    elif "Type::Study" in labels:
        errors.extend(check_label_unicity(issue, labels, "StatusS", "StatusS::New"))
    elif "Type::Task" in labels:
        errors.extend(check_label_unicity(issue, labels, "StatusT", "StatusT::New"))
    else:  # Type::UserStory
        errors.extend(check_label_unicity(issue, labels, "StatusU", "StatusU::New"))
    if (state == "closed") and not has_final_code_status(labels):
        errors.append("issue is closed but it has no final status")
    elif (state != "closed") and has_final_code_status(labels):
        errors.append("issue has a final status but it is not closed")
    if (
        has_status(labels)
        and not has_initial_code_status(labels)
        and not has_canceled_code_status(labels)
        and not has_suspended_code_status(labels)
    ):
        if not milestone:
            errors.append("issue is started but it has no milestone")
        if (modifiedOn or createdOn) > "2021-12-15":
            errors.extend(check_label_unicity(issue, labels, "Release"))
    if has_initial_code_status(labels) and mrCount > 0:
        errors.append(f"issue is not started but has {mrCount} MR")
    return errors


def check_doc_issue(issue, state, milestone, labels, createdOn, modifiedOn, mrCount):
    errors = []
    errors.extend(check_label_unicity(issue, labels, "Status", "Status::New"))
    if (state == "closed") and not has_final_doc_status(labels):
        errors.append("issue is closed but it has no final status")
    elif (state != "closed") and has_final_doc_status(labels):
        errors.append("issue has a final status but it is not closed")
    if (
        has_status(labels)
        and not has_initial_doc_status(labels)
        and not has_canceled_doc_status(labels)
        and not has_suspended_doc_status(labels)
        and ((modifiedOn or createdOn) > "2021-12-15")
    ):
        if not milestone:
            errors.append("issue is started but it has no milestone")
        errors.extend(check_label_unicity(issue, labels, "Release"))
    if has_initial_doc_status(labels) and mrCount > 0:
        errors.append(f"issue is not started but has {mrCount} MR")
    return errors


def print_issue_errors_as_html(
    report,
    project,
    id,
    title,
    url,
    milestone,
    createdBy,
    createdOn,
    modifiedBy,
    modifiedOn,
    errors,
):
    report.writelines(
        f"{project} - <A href='{url}' target='_blank'>#{id} {html.escape(title)}</A>\n"
    )
    if milestone:
        report.writelines(f"<BR>for milestone {html.escape(milestone)}\n")
    report.writelines(
        f"<BR>created by {html.escape(createdBy['name'])} (<A href='{createdBy['webUrl']}' target='_blank'>@{html.escape(createdBy['username'])}</A>) on {createdOn}\n"
    )
    if modifiedBy:
        report.writelines(
            f"<BR>last modified by {html.escape(modifiedBy['name'])} (<A href='{modifiedBy['webUrl']}' target='_blank'>@{html.escape(modifiedBy['username'])}</A>) on {modifiedOn}\n"
        )
    report.writelines("<UL>\n")
    for error in errors:
        report.writelines(f"<LI>{html.escape(error)}</LI>\n")
    report.writelines("</UL>\n<HR>\n")


def print_issue_errors_as_text(
    report,
    project,
    id,
    title,
    url,
    milestone,
    createdBy,
    createdOn,
    modifiedBy,
    modifiedOn,
    errors,
):
    report.writelines(f"{project} - #{id} {title}\n")
    if milestone:
        report.writelines(f"for milestone {milestone}\n")
    report.writelines(
        f"created by {createdBy['name']} (@{createdBy['username']}) on {createdOn}\n"
    )
    if modifiedBy:
        report.writelines(
            f"last modified by {modifiedBy['name']} (@{modifiedBy['username']}) on {modifiedOn}\n"
        )
    for error in errors:
        report.writelines(f"- {error}\n")
    report.writelines(
        "--------------------------------------------------------------------------------\n"
    )


def check_project_issues(
    html_report, html_string, text_string, culprit_file, projects, check_issue
):
    hasError = False
    for project in projects:
        result = run_query(QUERY % project)
        if result["data"]["project"] is None:
            print(f"Project {project} is empty")
            continue
        for issue in result["data"]["project"]["issues"]["edges"]:
            node = issue["node"]
            iid = node["iid"]
            title = node["title"]
            state = node["state"]
            url = node["webUrl"]
            labels = set(map(lambda x: x["node"]["title"], node["labels"]["edges"]))
            milestone = node["milestone"]["title"] if node["milestone"] else None
            createdBy = node["author"]
            if CREATORS and createdBy.get("username") not in CREATORS:
                continue
            createdOn = node["createdAt"]
            modifiedBy = node["updatedBy"]
            modifiedOn = node["updatedAt"]
            mrCount = node["mergeRequestsCount"]
            errors = check_issue(
                node, state, milestone, labels, createdOn, modifiedOn, mrCount
            )
            if errors:
                commons = [
                    project,
                    iid,
                    title,
                    url,
                    milestone,
                    createdBy,
                    createdOn,
                    modifiedBy,
                    modifiedOn,
                    errors,
                ]
                if html_report:
                    print_issue_errors_as_html(html_report, *commons)
                if html_string:
                    print_issue_errors_as_html(html_string, *commons)
                if text_string:
                    print_issue_errors_as_text(text_string, *commons)
                if culprit_file:
                    if not hasattr(check_project_issues, "culprits"):
                        check_project_issues.culprits = set()
                    culprit = (modifiedBy or createdBy)["username"]
                    if culprit not in check_project_issues.culprits:
                        culprit_file.writelines(f"{culprit}\n")
                    check_project_issues.culprits.add(culprit)
                hasError = True
    return hasError


if args.htmlreport:
    html_report = open(args.htmlreport, "w", encoding="utf8")
    html_report.write(
        """\
    <!DOCTYPE html>
    <html>
      <head>
        <META http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Issue report</title>
      </head>
      <body>
    """
    )
else:
    html_report = None

if args.htmlstring:
    html_string = open(args.htmlstring, "w", encoding="utf8")
else:
    html_string = None

if args.textstring:
    text_string = open(args.textstring, "w", encoding="utf8")
else:
    text_string = None

if args.culpritfile:
    culprit_file = open(
        args.culpritfile, "w", encoding="utf8", newline="\n"
    )  # newline='\n' is necessary when running this script on Windows, otherwise the bash files using the culprit file will have trouble
else:
    culprit_file = None

hasError = check_project_issues(
    html_report, html_string, text_string, culprit_file, code_projects, check_code_issue
)
hasError |= check_project_issues(
    html_report, html_string, text_string, culprit_file, doc_projects, check_doc_issue
)

if args.htmlreport:
    html_report.write(
        """\
    </body>
    </html>"""
    )
    html_report.close()
    print(f"Generated HTML report in {args.htmlreport}")

if args.htmlstring:
    html_string.close()
    print(f"Generated HTML string in {args.htmlstring}")

if args.textstring:
    text_string.close()
    print(f"Generated text string in {args.textstring}")

if args.culpritfile:
    culprit_file.close()
    print(f"Generated culprit list in {args.culpritfile}")

if args.interactive:
    if args.htmlreport:
        webbrowser.open("file://" + os.path.abspath(args.htmlreport))
    else:
        print("No HTML report has been generated, -i cannot be used")

sys.exit(1 if hasError else 0)
