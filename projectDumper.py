import html
import requests
import sys
import tempfile
import webbrowser
from projectLists import code_projects, doc_projects

if len(sys.argv) != 2:
    print(f'syntax: {sys.argv[0]} <token>')
    exit(1)
token = sys.argv[1]

headers = { "Authorization": token }

def run_query(query):
    request = requests.post("https://gitlab.com/api/graphql", json={'query': query}, headers = headers)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception("Query failed to run by returning code of {}. {}".format(request.status_code, query))
  
projects = code_projects + doc_projects

query = """
{
  project(fullPath: "%s") {
    webUrl
    containerRegistryEnabled
    description
    issuesEnabled
    jobsEnabled
    labels { edges { node { title description } } }
    lfsEnabled
    mergeRequestsFfOnlyEnabled
    printingMergeRequestLinkEnabled
    removeSourceBranchAfterMerge
    repository { rootRef }
    requestAccessEnabled
    serviceDeskEnabled
    sharedRunnersEnabled
    snippetsEnabled
    visibility
    wikiEnabled
  }
}
"""

def extract_scalar(fields, name, value):
    fields[name] = str(value)

def extract_list(fields, name, value):
    if not (name.endswith(".edges")):
        raise ValueError("List " + name + " should be named 'edges'")
    st = ""
    for element in value:
        for field in element["node"]:
            st += field + ": " + ("" if element["node"][field] is None else element["node"][field]) + "\n"
        st += "\n"
    fields[name[:-6]] = st

def extract_dict(fields, name, value):
    prefix = (name + ".") if name else ""
    for field in value:
        if type(value[field]) in (bool, str, type(None)):
            extract_scalar(fields, prefix + field, value[field])
        elif type(value[field]) is dict:
            extract_dict(fields, prefix + field, value[field])
        elif type(value[field]) is list:
            extract_list(fields, prefix + field, value[field])
        else:
            raise ValueError("Unsupported type " + str(type(value[field])) + " for " + field)


everything = {}
parsed_projects = []
for project in projects:
    result = run_query(query % project)
    if result["data"]["project"] is None:
        print(f"failed to retrieve data for project {project}")
        continue
    d = {}
    extract_dict(d, "", result["data"]["project"])
    parsed_projects.append(project)
    everything[project] = d

report = tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", suffix=".html", delete=False)
report.write("""
<!DOCTYPE html>
<html>
<head>
  <META http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style>
    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
      text-align:left;
      vertical-align:top;
    }
    th {
      background: white;
      position: sticky;
      top: 0;
}
  </style>
  <title>Project overview</title>
</head>
<body>
<table>
  <thead>
    <tr>
      <th></th>
""")
for project in parsed_projects:
    report.write("      <th>" + html.escape(project) + "</th>\n")
report.write(""""
    </tr>
  </thead>
  <tbody>
""")
for f in everything[parsed_projects[0]]:
    report.write("    <tr>\n")
    report.write("      <td>" + html.escape(f) + "</td>\n")
    for project in parsed_projects:
        report.write("      <td>" + html.escape(everything[project][f]).replace("\n","<br/>").replace("\r","") + "</td>\n")
    report.write("    </tr>\n")
report.write(""""
  </tbody>
</table>
</body>
</html>
""")

webbrowser.open('file://' + report.name)
