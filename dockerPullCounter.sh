#!/bin/bash

login="$1"
email="$2"
token="$3"
repo="gitlab.com/m4711"
project="data-store"
datafile="dockerPullCounter.csv"

git clone --depth 1 "https://$login:$token@$repo/$project.git"
python dockerPullCounter.py  squashtest/squash-tm squashtest/squash-orchestrator opentestfactory/orchestrator >> $project/$datafile
cd "$project"
git config --local user.email "$email"
git config --local user.name "$login"
git add $datafile
git commit -m "Update"
git push "https://$login:$token@$repo/$project.git"
cd ..
rm -fr "$project"
