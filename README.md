# GitLab-tools

Quick 'n dirty tools for GitLab usage

## clocgit.sh
Count the number of lines in each Git repo

## issueChecker.py
Check that issues are properly labelled  
- generates a HTML report
- can be used interactively, in which case the report is displayed in the Browser

## projectDumper.py
List parameters of the projects  
- generates a HTML report
- used interactively, displays the report in the Browser

## labels.sh
Create labels in a project  
!! Edit file before executing it so it does what you want! !!

## checklist.py
Create issues for tracking all tasks necessary for a TM/TF release

## board.py
Create an issue board.  
WIP, do not use

## setOpentestfactoryBoard.py
Configure the issue board for https://gitlab.com/opentestfactory

## dockerPullCounter.py
Extract the number of pulls of some Docker images from https://hub.docker.com/

