#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Syntax: $0 <project ID> <access token>"
    exit 2
fi

project_id=$1
access_token=$2

create_label() {
    curl --data-urlencode name="$1" \
         --data-urlencode description="$2" \
         --data-urlencode color="$3" \
         --data-urlencode priority="$4" \
         --header "PRIVATE-TOKEN: $access_token" \
         "https://gitlab.com/api/v4/projects/$project_id/labels"
}

# ------------------------------------------------------------------------------------------------
# labels for implementation projects (uncomment the following lines if applicable)

create_label "Type::UserStory"           "User story"                            "#0000ff"
create_label "Type::Bug"                 "Bug"                                   "#0000ff"
create_label "Type::Task"                "Task"                                  "#0000ff"
create_label "Type::Study"               "Study"                                 "#0000ff"
create_label "StatusU::New"              "Implementation can be started"         "#556b2f" "1001"
create_label "StatusU::ImplemInProgress" "Implementation in progress"            "#556b2f" "1002"
create_label "StatusU::ReadyForDemo"     "Implementation can be demoed to PO"    "#556b2f" "1003"
create_label "StatusU::ImplemFinished"   "Implementation accepted by PO"         "#556b2f" "1004"
create_label "StatusU::ReadyForTest"     "Tests can be started"                  "#556b2f" "1005"
create_label "StatusU::TestInProgress"   "Tests in progress"                     "#556b2f" "1006"
create_label "StatusU::ToBeCorrected"    "Defect(s) found in implementation"     "#556b2f" "1007"
create_label "StatusU::Validated"        "User story finished"                   "#556b2f" "1008"
create_label "StatusU::Suspended"        "User story suspended"                  "#556b2f" "1009"
create_label "StatusU::Cancelled"        "User story cancelled"                  "#556b2f" "1010"
create_label "StatusB::New"              "Fix can be started"                    "#32cd32" "1101"
create_label "StatusB::FixInProgress"    "Fix in progress"                       "#32cd32" "1102"
create_label "StatusB::ReadyForTest"     "Fix can be tested"                     "#32cd32" "1103"
create_label "StatusB::TestInProgress"   "Tests in progress"                     "#32cd32" "1104"
create_label "StatusB::TestKo"           "Defect(s) found in fix"                "#32cd32" "1105"
create_label "StatusB::Validated"        "Fix validated"                         "#32cd32" "1106"
create_label "StatusB::Feedback"         "Bug needs feedback"                    "#32cd32" "1107"
create_label "StatusB::Suspended"        "Bug suspended"                         "#32cd32" "1108"
create_label "StatusB::Cancelled"        "Bug cancelled"                         "#32cd32" "1109"
create_label "StatusT::New"              "Task can be started"                   "#9acd32" "1201"
create_label "StatusT::InProgress"       "Task in progress"                      "#9acd32" "1202"
create_label "StatusT::Done"             "Task done"                             "#9acd32" "1203"
create_label "StatusT::Suspended"        "Task suspended"                        "#9acd32" "1204"
create_label "StatusT::Cancelled"        "Task cancelled"                        "#9acd32" "1205"
create_label "StatusS::New"              "Study can be started"                  "#808000" "1301"
create_label "StatusS::InProgress"       "Study in progress"                     "#808000" "1302"
create_label "StatusS::Finished"         "Study finished"                        "#808000" "1303"
create_label "StatusS::ToRework"         "Study needs rework"                    "#808000" "1304"
create_label "StatusS::Validated"        "Study validated"                       "#808000" "1305"
create_label "StatusS::Suspended"        "Study suspended"                       "#808000" "1306"
create_label "StatusS::Cancelled"        "Study cancelled"                       "#808000" "1307"
create_label "Priority::Highest"         "Highest priority"                      "#ff0000" "1"
create_label "Priority::High"            "High priority"                         "#ff4400" "2"
create_label "Priority::Medium"          "Medium priority"                       "#ff8800" "3"
create_label "Priority::Low"             "Low priority"                          "#ffbb00" "4"
create_label "Priority::Lowest"          "Lowest priority"                       "#ffe000" "5"
create_label "ProdImpact::Yes"           "Exists in production"                  "#9400d3"
create_label "ProdImpact::No"            "Does not exist in production"          "#9400d3"
create_label "ProdImpact::ToBeAnalyzed"  "Impact on production to be analyzed"   "#9400d3"

# ------------------------------------------------------------------------------------------------
# labels for documentation projects (uncomment the following lines if applicable)

# create_label "Status::New"        "Documentation can be started" "#32cd32" "1001"
# create_label "Status::InProgress" "Documentation in progress"    "#32cd32" "1002"
# create_label "Status::Done"       "Documentation finished"       "#32cd32" "1003"
# create_label "Status::Suspended"  "Documentation suspended"      "#32cd32" "1004"
# create_label "Status::Cancelled"  "Documentation cancelled"      "#32cd32" "1005"

# ------------------------------------------------------------------------------------------------
# labels for communication projects (uncomment the following lines if applicable)

#create_label "Status::Submitted"       "Task waiting for approval"          "#32cd32" "1001"
#create_label "Status::Approved"        "Task approved and can be started"   "#32cd32" "1002"
#create_label "Status::InProgress"      "Task in progress"                   "#32cd32" "1003"
#create_label "Status::ReadyForValid"   "Task finished and can be validated" "#32cd32" "1004"
#create_label "Status::ValidInProgress" "Task validation in progress"        "#32cd32" "1005"
#create_label "Status::Validated"       "Task validated"                     "#32cd32" "1006"
#create_label "Status::ToRework"        "Task needs rework"                  "#32cd32" "1007"
#create_label "Status::Suspended"       "Task suspended"                     "#32cd32" "1008"
#create_label "Status::Cancelled"       "Task cancelled"                     "#32cd32" "1009"
