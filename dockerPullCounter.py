import datetime
import requests
import sys

def run_query(image):
    request = requests.get(f"https://hub.docker.com/v2/repositories/{image}/")
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception(f"Query failed with code of {request.status_code} for image '{image}'")

def get_pull_count(image):
    payload = run_query(image)
    return payload["pull_count"] 

script, *images = sys.argv
result = str(datetime.datetime.now(datetime.timezone.utc))
for image in images:
    result += "," + str(get_pull_count(image))
print(result)
