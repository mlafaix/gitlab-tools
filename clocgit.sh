#!/bin/bash

function clocARepo() {
  cd /tmp
  echo === "$1" ===
  git clone --quiet --depth 1 "$1" _git_tempo &&
  docker run --rm -v $PWD:/tmp aldanial/cloc _git_tempo
  rm -rf _git_tempo
}

repos=("agent"
       "java-param-library"
       "java-toolkit"
       "jenkins-plugin"
       "orchestrator"
       "python-toolkit"
       "qualitygate"
       "schemas"
       "site"
       "tools"
      )
for repo in ${repos[*]}; do
     clocARepo https://gitlab.com/opentestfactory/${repo}.git
done