import smtplib
import sys
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# -- command line parsing and setup
if len(sys.argv) < 7:
    print(f'syntax: {sys.argv[0]} <SMTP2GO user> <SMTP2GO password> <title> <HTML payload> <sender> <recipient 1> <recipient 2> <recipient 3> ...')
    exit(1)
username = sys.argv[1]
password = sys.argv[2]
title = sys.argv[3]
html_file = sys.argv[4]
sender = sys.argv[5]
recipients = sys.argv[6:]

msg = MIMEMultipart('mixed')

with open(html_file, "r", encoding="utf8") as f: html_string = f.read()

msg['Subject'] = title
msg['From'] = sender
msg['To'] = ", ".join(recipients)

html_message = MIMEText(html_string, 'html')
msg.attach(html_message)

mailServer = smtplib.SMTP('mail.smtp2go.com', 2525)
mailServer.ehlo()
mailServer.starttls()
mailServer.ehlo()
mailServer.login(username, password)
mailServer.sendmail(sender, recipients, msg.as_string())
mailServer.close()